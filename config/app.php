<?php

if (! function_exists('route_base')) {

    function route_base()
    {
        return '';
    }
}

if (! function_exists('config')) {

    function config($index)
    {
        $data = [
            'env' => env('APP_ENV', 'local'),
            'filiation' => '10001045',
            'token' => 'ba8cab98bd424dd1991a651393fc33e6',
            'route_base' => route_base()
        ];

        $data['erede_url'] = env('APP_ENV', 'local') == 'local' ?  'https://api.userede.com.br/desenvolvedores/v1/' : 'https://api.userede.com.br/erede/v1/';

        return $data[$index];
    }
}
