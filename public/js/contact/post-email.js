$(document).ready(function () {
    $('#btn-send-contact').on('click', function () {
        var $emptyFields = $('form.contactForm input:required').filter(function () {
            return $.trim(this.value) === "";
        });

        console.log($(this).data('route'));

        if (!$emptyFields.length) {
            $route = $(this).data('route');

            var $this = $(this);
            // DEsabilitando Botão
            var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Processando...';
            $this.attr('disabled', true);

            if ($(this).html() !== loadingText) {
                $this.data('original-text', $(this).html());
                $this.html(loadingText);
            }

            try {
                data = getInputToPost();

                $.post($route, data, function (response) {
                    console.log(response);
                    if(response.status == true) {
                        successMessage();
                    } else {
                        customErrorMessage(response.message);
                    }

                    $this.html($this.data('original-text'));
                })
                    .fail(function (err) {
                        var response = err.responseJSON,
                            errorMessage = '';

                        // console.log(response);
                        for (i in response.message) {
                            errorMessage += 'Code: ' + response.message[i].Code + ' | Message: ' +  response.message[i].Message + ", ";
                        }
                        // var message = translateRedeItauMessage(response.message.returnCode);

                        customErrorMessage(errorMessage);
                    })
            } catch (err) {
                console.log(err);
                errorMessage();
            }

            return false;
        }
    });

    function getInputToPost() {
        return {
            "page": $('input[name=page]').val(),
            "name": $('input[name=name]').val(),
            "phone": $('input[name=phone]').val(),
            "email": $('input[name=email]').val(),
            "faleMais": $('textarea[name=faleMais]').val(),
        };
    }

    function errorMessage() {
        swal({
                title: "Ocorreu um Erro!",
                text: "Tente novamente ou entre em Contato com o Administrador do Sistema!",
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Tentar Novamente!",
                closeOnConfirm: true
            },
            function () {
                location.reload();
            });
    };

    function successMessage() {
        swal({
                title: "Obrigado!",
                text: "Recebemos seu Email!",
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "OK!",
                closeOnConfirm: true
            },
            function () {
                location.reload();
            });
    };

    function customErrorMessage(text) {
        swal({
                title: "Ocorreu um Erro!",
                text: text,
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Tentar Novamente!",
                closeOnConfirm: true
            },
            function () {
                location.reload();
            });
    };
});