<?php
/**
 * https://stackoverflow.com/questions/38686776/how-do-i-use-fastroute
 */
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
//    $r->addRoute('GET', '/assets', \ERede\Controllers\CreditController::class . '/assets');

    $r->addRoute('GET', route_base() . '/produtos/sna', \Eleven\Controllers\PageController::class . '/sna');
    $r->addRoute('GET', route_base() . '/produtos/bi', \Eleven\Controllers\PageController::class . '/bi');
    $r->addRoute('POST', route_base() . '/send-mail', \Eleven\Controllers\MailerController::class . '/send');

});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo json_response(404, 'Página não encontrada.');
        die;

        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo json_response(405, 'Método Http não permitido para esta rota.');
        die;

        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);

        break;
}

