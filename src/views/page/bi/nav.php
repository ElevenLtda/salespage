<!-- Nav -->
<header>
    <nav id="nav" class="navbar navbar-expand-md navbar-light fixed-top">
        <a class="navbar-brand" href="#"></a>
        <span class="img-logo">BI</span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto custom-nav">
                <li class="nav-item pr-4 active">
                    <a id="menu-inicio" class="nav-link" href="#">ÍNICIO
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item pr-4">
                    <a id="menu-sobre" class="nav-link" href="#">SOBRE</a>
                </li>
                <li class="nav-item pr-4">
                    <a id="menu-funcionalidades" class="nav-link" href="#">FUNCIONALIDADES</a>
                </li>
                <li class="nav-item pr-4">
                    <a id="menu-contact" class="nav-link" href="#">CONTATO</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- /.Nav -->