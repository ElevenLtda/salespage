<!--Footer-->
<footer class="page-footer font-small bg-footer text-white lighten-5 pt-0">

    <div class="bg-black-active">
        <div class="container justify-content-center">
            <!--Grid row-->
            <div class="row py-4 d-flex align-items-center">

                <!--Grid column-->
<!--                <div class="col-12 col-md-5 text-left mb-4 mb-md-0">-->
<!--                    <h6 class="mb-0 white-text text-center text-md-left">-->
<!--                        <strong>Acesse nossas Redes Sociais!</strong>-->
<!--                    </h6>-->
<!--                </div>-->
                <!--Grid column-->

                <!--Grid column-->
<!--                <div class="col-12 col-md-7 text-center text-md-right">-->
<!--                    <!--Facebook-->
<!--                    <a class="fb-ic ml-0">-->
<!--                        <i class="fa fa-facebook white-text mr-lg-4"> </i>-->
<!--                    </a>-->
<!--                    <!--Twitter-->
<!--                    <a class="tw-ic">-->
<!--                        <i class="fa fa-twitter white-text mr-lg-4"> </i>-->
<!--                    </a>-->
<!--                    <!--Google +-->
<!--                    <a class="gplus-ic">-->
<!--                        <i class="fa fa-google-plus white-text mr-lg-4"> </i>-->
<!--                    </a>-->
<!--                    <!--Linkedin-->
<!--                    <a class="li-ic">-->
<!--                        <i class="fa fa-linkedin white-text mr-lg-4"> </i>-->
<!--                    </a>-->
<!--                    <!--Instagram-->
<!--                    <a class="ins-ic">-->
<!--                        <i class="fa fa-instagram white-text mr-lg-4"> </i>-->
<!--                    </a>-->
<!--                </div>-->
                <!--Grid column-->

            </div>
            <!--Grid row-->
        </div>
    </div>

    <!--Footer Links-->
    <div class="container mt-5 mb-4">
        <div class="row mt-3">

            <!--First column-->
            <div class="col-md-6 col-lg-6 col-xl-6 mb-4 text-white">
                <h6 class="text-uppercase">
                    <strong>Fale Conosco</strong>
                </h6>
                <form class="contactForm">

                    <input type="hidden" name="page" value="BI">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name"
                                       aria-describedby="emailHelp" placeholder="Digite o Nome" required>
                                <small id="emailHelp" class="form-text text-muted">Seu nome ou nome da Instituição
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" name="phone"
                                       aria-describedby="phone" placeholder="Digite o Telefone" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" name="email"
                                       aria-describedby="email" placeholder="Digite o Email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <textarea class="form-control" id="faleMais" aria-describedby="faleMais" name="faleMais"
                                          placeholder="Fale mais..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row text-right">
                        <div class="col-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-primary" data-route="/send-mail" id="btn-send-contact">ENVIAR</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-3 col-lg-3 col-xl-3 mb-4 text-white">
                <h6 class="text-uppercase">
                    <strong>Produtos</strong>
                </h6>
                <hr class="accent-3 mb-4 mt-0" style="width: 60px;">
                <p>
                    <a href="/produtos/sna" class="text-white">SNA</a><br />
                    <small>Sistema de Notificação de Agravos</small>
                </p>
                <p>
                    <a href="/produtos/bi" class="text-white">BI - eSus</a><br />
                    <small>Business Intelligence</small>
                </p>
                <!-- <p>
                    <a href="#!" class="text-white">MDWordPress</a>
                </p>
                <p>
                    <a href="#!" class="text-white">BrandFlow</a>
                </p>
                <p>
                    <a href="#!" class="text-white">Bootstrap Angular</a>
                </p> -->
            </div>
            <!--/.Second column-->

            <!--Fourth column-->
            <div class="col-md-3 col-lg-3 col-xl-3 text-white">
                <h6 class="text-uppercase">
                    <strong>Contatos</strong>
                </h6>
                <hr class="accent-3 mb-4 mt-0" style="width: 60px;">
                <p><i class="fa fa-home mr-3"></i> Palmas, TO - Brasil</p>
                <p><i class="fa fa-envelope mr-3"></i> saude@consisti.net.br</p>
                <p><i class="fa fa-whatsapp mr-3"></i> (63) 9 8413-9235 </p>
<!--                <p><i class="fa fa-phone mr-3"></i> (63) 3215-0806</p>-->
            </div>
            <!--/.Fourth column-->

        </div>
    </div>
    <!--/.Footer Links-->

    <!-- Copyright-->
    <div class="footer-copyright py-3 text-center">
        © 2018 Copyright:
        <strong> CONSISTI | ELEVEN</strong>
    </div>
    <!--/.Copyright -->

</footer>
<!--/.Footer-->