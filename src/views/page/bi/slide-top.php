<!-- slide -->
<div id="carouselSYSIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators" style="margin-bottom: 20px">
        <li data-target="#carouselSYSIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselSYSIndicators" data-slide-to="1"></li>
        <li data-target="#carouselSYSIndicators" data-slide-to="2"></li>
        <li data-target="#carouselSYSIndicators" data-slide-to="3"></li>
        <li data-target="#carouselSYSIndicators" data-slide-to="4"></li>
    </ol>

    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="<?= $routeAssets . '/img/bi/banner1.jpg' ?>" alt="First slide" alt="banner1">
            <div class="carousel-caption d-none d-md-block text-left">
                <h1 class="font-weight-bold">Um Sistema</h1>
                <p>que extrai, analisa e sintetiza informações presentes no sistema e-SUS do Ministério
                    da Saúde.</p>
            </div>
            <!--                            <div class="carousel-caption d-none d-md-block text-left">-->
            <!--                                <h1 class="font-weight-bold">Um Sistema</h1>-->
            <!--                                <p>que extrai, analisa e sintetiza informações presentes no sistema e-SUS do Ministério-->
            <!--                                    da Saúde.</p>-->
            <!--                            </div>-->
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= $routeAssets . '/img/bi/banner2.jpg' ?>" alt="First slide" alt="banner2">
            <div class="carousel-caption d-none d-md-block text-left">
                <h1 class="font-weight-bold">Foi desenvolvido</h1>
                <p>a partir a da necessidade de se analisar informações do sistema e-SUS que não são
                    disponibilizadas por meio de qualquer relatório presente nesse sistema atualmente.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= $routeAssets . '/img/bi/banner3.jpg' ?>" alt="First slide" alt="banner3">
            <div class="carousel-caption d-none d-md-block text-left">
                <h1 class="font-weight-bold">Além disso, viabiliza a exportação de dados </h1>
                <p>em formatos que permitam a construção de indicadores epidemiológicos, operando de
                    forma integrada com os sistemas de informação do SUS além de instrumentalizar com
                    tecnologias da informação a governança dos serviços de saúde.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= $routeAssets . '/img/bi/banner4.jpg' ?>" alt="First slide" alt="banner4">
            <div class="carousel-caption d-none d-md-block text-left">
                <h1 class="font-weight-bold">O conjunto de informações </h1>
                <p>providos por essa ferramenta constitui um excelente material para apoio à tomada de
                    decisões estratégicas e de gestão em saúde pública.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= $routeAssets . '/img/bi/banner5.jpg' ?>" alt="First slide" alt="banner5">
            <div class="carousel-caption d-none d-md-block text-left">
                <h1 class="font-weight-bold">A partir da leitura e análise dos dados do e-SUS</h1>
                <p>essa ferramenta de BI oferece um grande conjunto de relatórios gerenciais que sintetizam
                    e apresentam informações em gráficos e tabelas que permitem uma visão mais abrangente
                    de diversas métricas.</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselSYSIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselSYSIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
</div>
</div>
</div>
<!-- /.slide -->