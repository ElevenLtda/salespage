<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126744321-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-126744321-1');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BI - Business Inteligence</title>
    <meta name="description" content="O BI da Eleven é um Sistema que extrai, analisa e sintetiza informações presentes no sistema e-SUS do Ministério da Saúde">

    <!-- og -->
    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="https://saude.consisti.net.br/produtos/bi">
    <meta property="og:title" content="BI - Business Inteligence">
    <meta property="og:site_name" content="Eleven BI (Business Inteligence)">
    <meta property="og:description" content="O BI da Eleven é um Sistema que extrai, analisa e sintetiza informações presentes no sistema e-SUS do Ministério da Saúde">
    <meta property="og:image" content="<?= $routeAssets . '/img/favicon.ico' ?>">
    <meta property="og:image:type" content="image/x-icon">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="500">
    <meta property="og:type" content="website">
    <!-- og -->

    <link rel="icon" href="<?= $routeAssets . '/img/favicon.ico' ?>">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?= $routeAssets . '/css/bootstrap/bootstrap.min.css' ?>">
    <link rel="stylesheet" href="<?= $routeAssets . '/css/bootstrap/bootstrap-validator.css' ?>"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet">
<!--    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">-->
    <link href="<?= $routeAssets . '/css/bi.css' ?>" rel="stylesheet">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/icons/bi/style.css' ?>">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/sweetalert.css' ?>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php require_once 'nav.php' ?>

    <?php require_once 'slide-top.php' ?>

    <?php require_once 'caracteristicas.php' ?>

    <?php require_once 'funcionalidades.php' ?>



    <!-- contato -->
    <div class="py-5 text-left bg-contact" id="ctn-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 pb-3">
                            <h1 class="text-secondary">O BI vai facilitar a Gestão da Saúde do seu Município!</h1>
                        </div>
                    </div>
<!--                    <a href="#" class="btn btn-custom-primary">Entre em Contato</a>-->
<!--                    <a href="#" class="btn btn-custom-secondary">Solicite um Orçamento</a>-->
                </div>
            </div>
        </div>
    </div>
    <!-- Fim Contato -->

    <?php require_once 'footer.php' ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= $routeAssets . '/js/jquery3.3.1.min.js' ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous">
    </script>
    <script src="<?= $routeAssets . '/js/bootstrap/bootstrap.min.js' ?>"></script>
    <script type="text/javascript" src="<?= $routeAssets . '/js/bootstrap/bootstrap-validator.js' ?>"></script>
    <script type="text/javascript" src="<?= $routeAssets . '/js/jquery-mask/jquery.mask.min.js' ?>"></script>
    <script type="text/javascript" src="<?= $routeAssets . '/js/language/pt_BR.js' ?>"></script>
    <script type="text/javascript" src="<?= $routeAssets . '/js/validator/contact.js' ?>"></script>
    <script src="<?= $routeAssets . '/js/sweetalert.min.js' ?>"></script>
    <script src="<?= $routeAssets . '/js/contact/post-email.js' ?>"></script>

    <script type="text/javascript">

        $("#menu-inicio").click(function () {
            $('html, body').animate({
                scrollTop: $("#carouselSYSIndicators").offset().top
            }, 500);
        });

        $("#menu-sobre").click(function () {
            $('html, body').animate({
                scrollTop: $("#ctn-sobre").offset().top - 90
            }, 500);
        });

        $("#menu-funcionalidades").click(function () {
            $('html, body').animate({
                scrollTop: $("#ctn-funcionalidades").offset().top - 50
            }, 500);
        });

        $("#menu-contact").click(function () {
            $('html, body').animate({
                scrollTop: $("#ctn-contact").offset().top - 50
            }, 500);
        });
    </script>
</body>

</html>