<!-- funcionalidades -->
<div class="funcionalidades py-5 mt-5" id="ctn-funcionalidades">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <h2 class="mb-2 text-primary">FUNCIONALIDADES</h2>
                <p class="lead font-light ">Perfil do Profissional</p>
            </div>
        </div>
        <div class="row align-items-center py-2">
            <div class="col-md-9">
                <!-- <h2 class="text-primary pt-3">Eleven conta com 12 fichas investigativas:</h2> -->
                <ul class="p-2 cnt-list">
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Quantitativo populacional: por município, território, unidade de saúde, equipe e profissional cadastrante;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Quantitativo populacional para diabetes, hanseníase, acamado, tuberculose, gestantes, hipertensos e câncer, cada um com os
                        totais por município, território, unidade, equipe e profissional cadastrante;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Acesso a lista nominal dos cidadãos que se encontram em cada das condições de saúde mencionadas acima;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Quantitativo por domicílios: zona rural, casa própria, casa, apartamento, moradia própria, água encanada; esses totais informados
                        por município, território, unidade e equipe;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Relação de procedimentos realizados pelo profissional;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Identificação do profissional constando cpf, cns, cbo, território, unidade e equipe de lotação;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Permite navegar entre as múltiplas lotações do profissional;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Permite acesso à lista dos outros profissionais da mesma equipe;
                    </li>
                    <li class="py-1">
                        <span class="icon-check xp"></span>
                        Permite acesso à páginas de perfil dos profissionais da mesma equipe;
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <img 
                    class="img-fluid d-block mb-4 w-100 " 
                    src="<?= $routeAssets . '/img/bi/analytic.png' ?>"
                    alt="Ilustração"
                    >
            </div>
        </div>
    </div>
</div>
<!-- Fim de funcionalidades -->