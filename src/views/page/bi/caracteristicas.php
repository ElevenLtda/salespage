<!-- Caracteristicas -->
<div class="sobre-area" id="ctn-sobre">
    <div class="container">
        <div class="row sobre-items text-center">
            <div class="col-md-4">
                <div class="icon"><i class="icon-user" aria-hidden="true"></i></div>
                <div class="info">
                    Autenticação do usuário integrada à base de dados do e-sus
                </div>
            </div>
            <div class="col-md-4">
                <div class="icon"><i class="icon-id-card" aria-hidden="true"></i></div>
                <div class="info">
                    Acesso a diferentes perfis do usuário conforme registros no e-sus
                </div>
            </div>
            <div class="col-md-4">
                <div class="icon"><i class="icon-collaboration" aria-hidden="true"></i></div>
                <div class="info">
                    Áreas distintas para ao menos três tipos de perfis: profissional, coordenador da unidade e o
                    gestor municipal
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Caracteristicas -->