<!-- Nav -->
<header>
    <nav id="nav" class="navbar navbar-expand-md navbar-light fixed-top">
        <a class="navbar-brand text-secondary hi" href="#"><h1>SNA</h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto custom-nav">
                <li class="nav-item pr-4 active">
                    <a id="inicio" class="nav-link" href="#">ÍNICIO
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item pr-4 active">
                    <a id="rec-inc" class="nav-link" href="#">RECURSOS INCRÍVEIS
                    </a>
                </li>
                <li class="nav-item pr-4">
                    <a id="diferencial" class="nav-link" href="#">DIFERENCIAL</a>
                </li>
                <li class="nav-item pr-4">
                    <a id="ben-raz" class="nav-link" href="#">BENEFÍCIOS & RAZÕES</a>
                </li>
                <li class="nav-item pr-4">
                <a id="contact" class="nav-link" href="#">CONTATO</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- Nav -->