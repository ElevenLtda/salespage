<!-- Beneficios e Razões -->
<div class="container py-5" id="ctn-ben-raz">
    <div class="row text-center">
        <div class="col-md-12">
            <h1 class="mb-2 text-primary">BENEFÍCIOS & RAZÕES</h1>
            <p class="lead font-light ">PARA UTILIZAR O SISTEMA DE NOTIFICAÇÕES DE AGRAVOS</p>
        </div>
    </div>
    <div class="row align-items-center py-2">
        <div class="col-md-3">
            <img class="img-fluid d-block mb-4 w-100 " src="<?= $routeAssets . '/img/sna/actor.png' ?>"></div>
        <div class="col-md-9">
            <!-- <h2 class="text-primary pt-3">Eleven conta com 12 fichas investigativas:</h2> -->
            <ul class="p-2 cnt-list">
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Notificações podem ser acessadas de qualquer unidade de saúde
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Notificações em tempo real
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Mais agilidade e organização das notificações
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Economia de papel
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Gestão centralizada
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Diversos relatórios para apoio à tomada de decisão
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Dispensa a instalação de sistemas nas unidades de saúde
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Integração com e-sus para captura de dados
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Controle de acesso em diversos níveis
                </li>
                <li class="py-1">
                    <span class="icon-check xp"></span>
                    Sistema simples, leve e eficiente
                </li>
            </ul>
        </div>
    </div>
</div>