<!-- Fichas -->
<div class="container pb-5" id="ctn-diferencial">
    <div class="row align-items-center">
        <div class="col-md-5">
            <img class="img-fluid d-block mb-4 w-100" src="<?= $routeAssets . '/img/sna/mac.png' ?>">
        </div>
        <div class="col-md-7">
            <h2 class="text-primary pt-3">Além das notificações compulsórias, o sistema dispõe ainda das fichas para a investigação dos seguintes agravos:</h2>
            <ul class="p-0 cnt-list">
                <li>
                    <span class="icon-check xp"></span>
                    Hanseníase
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Sifilis Congênita
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Dengue
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Febre de Chikungunya
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    HIV/AIDS
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Leishmaniose Visceral
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Intoxicação Exógena
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Acidente por animal peçonhento
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Violência Interpessoal Autoprovocada
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Acidente de Trabalho com exposição a material biológico
                </li>
                <li>
                    <span class="icon-check xp"></span>
                    Atendimento Anti-rábico Humano
                </li>
            </ul>
        </div>
    </div>
</div>