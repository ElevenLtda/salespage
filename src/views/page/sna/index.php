<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126744321-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-126744321-1');
    </script>
    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sistema de Notificação de Agravos</title>
    <meta name="description" content="Sistema para notificação e investigação de agravos disponível a toda rede pública de saúde municipal ou estadual">
    <link rel="icon" href="<?= $routeAssets . '/img/favicon.ico' ?>">

    <!-- og -->
    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="https://saude.consisti.net.br/produtos/sna">
    <meta property="og:title" content="Sistema de Notificação de Agravos - Eleven">
    <meta property="og:site_name" content="Sistema de Notificação de Agravos - Eleven">
    <meta property="og:description" content="Sistema para notificação e investigação de agravos disponível a toda rede pública de saúde municipal ou estadual">
    <meta property="og:image" content="<?= $routeAssets . '/img/favicon.ico' ?>">
    <meta property="og:image:type" content="image/x-icon">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="500">
    <meta property="og:type" content="website">
    <!-- og -->

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700, 800" rel="stylesheet">
    <link rel="stylesheet" href="<?= $routeAssets . '/css/bootstrap/bootstrap.min.css' ?>">
    <link rel="stylesheet" href="<?= $routeAssets . '/css/bootstrap/bootstrap-validator.css' ?>"/>
    <link href="<?= $routeAssets . '/css/sna.css' ?>" rel="stylesheet">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/icons/sna/style.css' ?>">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/sweetalert.css' ?>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>


<?php require_once 'nav.php' ?>

<?php require_once 'rec-incriveis.php' ?>

<?php require_once 'fichas.php' ?>

<div class="py-5 text-center bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 pb-4">
                        <h1 class="text-secondary">O SNA vai facilitar a Gestão da Saúde do seu Município</h1>
                    </div>
                </div>
                <a href="#" id="cotacao" class="btn btn-custom-secondary">SOLICITE UMA COTAÇÃO</a>
            </div>
        </div>
    </div>
</div>

<?php require_once 'ben-razoes.php' ?>

<?php require_once 'footer.php' ?>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?= $routeAssets . '/js/jquery3.3.1.min.js' ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous">
</script>
<script src="<?= $routeAssets . '/js/bootstrap/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?= $routeAssets . '/js/bootstrap/bootstrap-validator.js' ?>"></script>
<script type="text/javascript" src="<?= $routeAssets . '/js/jquery-mask/jquery.mask.min.js' ?>"></script>
<script type="text/javascript" src="<?= $routeAssets . '/js/language/pt_BR.js' ?>"></script>
<script type="text/javascript" src="<?= $routeAssets . '/js/validator/contact.js' ?>"></script>
<script src="<?= $routeAssets . '/js/sweetalert.min.js' ?>"></script>
<script src="<?= $routeAssets . '/js/contact/post-email.js' ?>"></script>

<script type="text/javascript">

    var nav = document.getElementById('nav');

    window.onscroll = function () {

        if (window.pageYOffset > 100) {

            nav.style.background = "linear-gradient(180deg, rgba(42, 86, 158, 0.81) 31.15%, rgba(55, 128, 191, 0.81) 100%)";
            nav.style.boxShadow = "0px 1px 12px rgba(42, 86, 158, 0.81)";
        }
        else {
            nav.style.background = "transparent";
            nav.style.boxShadow = "none";
        }
    }

    $("#inicio").click(function() {
        $('html, body').animate({
            scrollTop: $("#ctn-inicio").offset().top
        }, 500);
    });

    $("#rec-inc").click(function() {
        $('html, body').animate({
            scrollTop: $("#ctn-rec-inc").offset().top - 90
        }, 500);
    });

    $("#diferencial").click(function() {
        $('html, body').animate({
            scrollTop: $("#ctn-diferencial").offset().top - 50
        }, 500);
    });

    $("#ben-raz").click(function() {
        $('html, body').animate({
            scrollTop: $("#ctn-ben-raz").offset().top - 50
        }, 500);
    });

    $("#contact").click(function() {
        $('html, body').animate({
            scrollTop: $("footer").offset().top
        }, 500);
    });

    $("#cotacao").click(function() {
        $('html, body').animate({
            scrollTop: $("footer").offset().top
        }, 500);
    });
</script>

</body>
</html>