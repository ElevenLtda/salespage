
<!-- Recursos Incriveis -->
<div class="container py-4">

    <div class="position-relative overflow-hidden p-3 p-md-5 text-center" id="ctn-inicio">
        <div class="col my-5">
            <h1 class="d-inline text-secondary">Sistema de Notificação de Agravos</h1>
            <p class="lead font-light py-2 text-secondary">
                Sistema para notificação e investigação de agravos disponível a toda rede pública de saúde municipal ou
                estadual
            </p>
        </div>
        <div class="row align-items-center">
            <div class="col">
                <img class="mac-book" src="<?= $routeAssets . '/img/sna/macbook2.png' ?>">
            </div>
        </div>
<!--        <div class="col" style="margin-top: -20px">-->
<!--            <a class="btn btn-custom-primary" href="#">CONHEÇA</a>-->
<!--        </div>-->
    </div>

    <div id="ctn-rec-inc" class="row text-center">
        <div class="col-md-12">
            <h1 class="mb-3 text-primary">RECURSOS INCRÍVEIS</h1>
            <p class="lead font-light"></p>
            <div class="row mt-5">

                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-notification d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary">
                                <b>Notificações de Agravos</b>
                            </h5>
                            <p class="text-center">Notificações de todos os agravos monitorados pelo Ministério da
                                Saúde</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-forms d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary text-center">
                                <b>Fichas Homologadas</b>
                            </h5>
                            <p class="text-center">Todas as fichas são homologadas pelo Ministério da Saúde</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-medical d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary text-center">
                                <b>Fichas de Investigação</b>
                            </h5>
                            <p class="text-center">Dos 12 principais agravos como a Hanseníase que possui o Registro
                                eletrônico da avaliação sensitiva</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-notification_mobile d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary text-center">
                                <b>Alerta Epidemiológico por SMS</b>
                            </h5>
                            <p class="text-center">Notificações de todos os agravos monitorados pelo Ministério da
                                Saúde</p>
                        </div>
                    </div>
                </div>
                <span class="sr-only">(current)</span>
                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-approval d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary text-center">
                                <b>Emissão de Boletins diários</b>
                            </h5>
                            <p class="text-center">A emissão é realizada de todos os agravos notificados no
                                sistema.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-3 font-light">
                    <div class="row mb-3">
                        <div class="col">
                            <span class="icon-geolocalization d-block mx-auto"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h5 class="text-secondary text-center">
                                <b>Geolocalização</b>
                            </h5>
                            <p class="text-center">Geolocalização dos casos e contatos automáticos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>