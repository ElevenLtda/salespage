<?php
/**
 * Created by PhpStorm.
 * User: horecio
 * Date: 12/09/18
 * Time: 16:58
 */

namespace Eleven;


trait FileTrait
{
    private function crateFile($folder, $name)
    {
        if (!file_exists($folder)) {
            mkdir($folder , 0777, true);
        }

        $pathFile = $folder . '/' . $name;
        $myfile = fopen($pathFile, "w") or die("Unable to open file!");

        return $this;
    }

    private function writeFile($folder, $name, $text)
    {
        $pathFile = $folder . '/' . $name;
        $myfile = fopen($pathFile, "w") or die("Unable to open file!");
        fwrite($myfile, $text);
        fclose($myfile);

        return true;
    }

}