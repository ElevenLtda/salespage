<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:05
 */

namespace Eleven\Controllers;

use GuzzleHttp\Exception\GuzzleException;

class PageController
{

    // https://lipis.github.io/bootstrap-sweetalert/
    public function sna()
    {
        $routeAssets = config('route_base');

        include view_path('page/sna/index.php');
    }

    // https://lipis.github.io/bootstrap-sweetalert/
    public function bi()
    {
//        dd(app_path('PHPMailer/email-logs'));
        $routeAssets = config('route_base');

        include view_path('page/bi/index.php');
    }
}
