<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:05
 */

namespace Eleven\Controllers;

use Eleven\FileTrait;
use Eleven\PHPMailer\Mailer;

class MailerController
{
    use FileTrait;

    public function send()
    {
        $mailer = new Mailer();

        $subject = sprintf('Contato da Page: %s', $_POST['page']);
        $body = sprintf('Nome: %s, Telefone: %s, Email: %s, Comentário: %s', $_POST['name'], $_POST['phone'], $_POST['email'], $_POST['faleMais']);

//        $nameFile = 'date("Y-m-d H:i:s")' . $_POST['page'];
        $nameFile = date("Y-m-d H-i") . 'PAGE-' . $_POST['page'] . '.log';
        $this->crateFile(app_path('Eleven/PHPMailer/email-logs'), $nameFile)
            ->writeFile(app_path('Eleven/PHPMailer/email-logs'), $nameFile, $body);

        $result = $mailer->setContent($subject, $body)
            ->send();

        $status = $result['success'] ? 200 : 500;

        echo json_response($status, $result['message']);
    }
}
