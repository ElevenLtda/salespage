<?php
/**
 * Created by PhpStorm.
 * User: horecio
 * Date: 12/09/18
 * Time: 13:03
 */

namespace Eleven\PHPMailer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer
{
    private $mail;

    /**
     * Mailer constructor.
     * @param $mail
     */
    public function __construct()
    {
        $this->mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    }

    protected function configure()
    {
        //Server settings
        $this->mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $this->mail->isSMTP();                                      // Set mailer to use SMTP
        $this->mail->Host = env('MAIL_HOST');  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = env('MAIL_USERNAME');                 // SMTP username
        $this->mail->Password = env('MAIL_PASSWORD');                           // SMTP password
        $this->mail->SMTPSecure = env('MAIL_ENCRYPTION');                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = env('MAIL_PORT');

        return $this;
    }

    protected function recipient()
    {
        //Recipients
        $this->mail->setFrom(env('MAIL_USERNAME'), 'Contato');
        $this->mail->addAddress('saude@consisti.net.br', 'ELEVEN');     // Add a recipient
        $this->mail->addAddress('horecio@gmail.com');               // Name is optional
        $this->mail->addAddress('flp.dsc@gmail.com');               // Name is optional
        $this->mail->addReplyTo('saude@consisti.net.br', 'Retorno de Contato');

        return $this;
    }

    public function setContent($subject, $body, $altBody = null)
    {
        //Content
        $this->mail->isHTML(true);                                  // Set email format to HTML
        $this->mail->Subject = $subject;
        $this->mail->Body    = $body;
        !$altBody ?: $this->mail->AltBody = $altBody;

        return $this;
    }

    public function send()
    {
        try {
            $this->configure()
                ->recipient();

            $this->mail->send();

            return ['success' => true, 'message' => 'Email Enviado com Sucesso.'];
        } catch (Exception $exception) {
            return [
                'success' => false,
                'message' => $exception->errorMessage()
            ];
         }
    }
}