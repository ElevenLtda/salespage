<?php
/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 30/07/18
 * Time: 00:03
 */

namespace Eleven;

trait ModelTrait
{
    public function fill(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if ($key == 'amount') {
                $this->$key = preg_replace("/\D+/", "", $value);
            } elseif ($key == 'cardNumber') {
                $this->$key = preg_replace("/\D+/", "", $value);
            } else {
                $this->$key = $value;
            }
        }

        return $this;
    }
}